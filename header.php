<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
		"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php wp_title('|', true, 'right'); ?></title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
	<div id="header">
		<h1><a href="<?php echo home_url('/'); ?>" title="St. Charles East Music Boosters">St. Charles East Music Boosters</a></h1>
		<ul id="meta">
		<?php foreach(wp_get_nav_menu_items(HEADER_META_NAV_MENU_ID) as $menu_item) : ?>
			<li><a href="<?php echo $menu_item->url; ?>"><i class="fa <?php echo implode(' ', $menu_item->classes); ?>" aria-hidden="true"></i> <?php echo $menu_item->title; ?></a></li>
		<?php endforeach; ?>
		</ul>
		<ul id="nav">
		<?php foreach(wp_get_nav_menu_items(HEADER_NAV_MENU_ID) as $menu_item) : ?>
			<li><a href="<?php echo $menu_item->url; ?>" <?php echo (is_front_page() || is_page('home')) ? 'class="current"' : '' ?>><?php echo $menu_item->title; ?></a></li>
		<?php endforeach; ?>
		</ul>
		<label for="hamburger_menu_checkbox" id="hamburger_menu_toggle" class="hamburger-menu-utility"><i class="fa fa-bars" aria-hidden="true"></i> Main Menu</label>
		<input type="checkbox" id="hamburger_menu_checkbox" name="hamburger-menu-checkbox">
		<ul class="hamburger-menu hamburger-menu-utility">
		<?php foreach(wp_get_nav_menu_items(HEADER_NAV_MENU_ID) as $menu_item) : ?>
			<li><a href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a></li>
		<?php endforeach; ?>
		<?php foreach(wp_get_nav_menu_items(HEADER_META_NAV_MENU_ID) as $menu_item) : ?>
			<li><a href="<?php echo $menu_item->url; ?>"><?php echo $menu_item->title; ?></a></li>
		<?php endforeach; ?>
		</ul>
	</div>