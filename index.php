<?php get_header(); ?>

<div class="banner-wrap">
    <?php
    $page = get_page_by_title('Home');
    $my_id = $page->ID;
    $post_id = get_post($my_id);
    $home = $post_id->post_content;
    $home = apply_filters('the_content', $home);
    $home = str_replace(']]>', ']]>', $home);
    ?>
    <div class="intro"><?php echo $home; ?></div>
    <div class="pic">
        <img src="<?php echo home_url('/'); ?>wp-content/uploads/band.png" />
        <img src="<?php echo home_url('/'); ?>wp-content/uploads/choir.jpg" />
        <img src="<?php echo home_url('/'); ?>wp-content/uploads/orchestra-updated.jpg" />
    </div>
</div>

<div id="content" class="index no-border">
	<a href="<?php echo home_url('/'); ?>band"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/button-band.jpg" /></a>
	<a href="<?php echo home_url('/'); ?>choir"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/button-choir.jpg" /></a>
	<a href="<?php echo home_url('/'); ?>orchestra"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/button-orchestra.jpg" /></a>
</div>

<?php get_footer(); ?>
