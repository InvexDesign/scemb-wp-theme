<div id="footer">
    <span class="info">St. Charles East Music Boosters &nbsp;&nbsp;&nbsp;//&nbsp;&nbsp;&nbsp; 1020 Dunham Rd, St. Charles, IL 60174 &nbsp;&nbsp;&nbsp;//&nbsp;&nbsp;&nbsp; <a href="mailto:stcemusicboosters@gmail.com">stcemusicboosters@gmail.com</a> &nbsp;&nbsp;&nbsp;//&nbsp;&nbsp;&nbsp; © <?php echo date("Y"); ?></span>
    <span class="invex">Powered by <a href="http://www.invexdesign.com" target="_blank" title="Invex Design | Web Design, Web Hosting, SEO Consulting, Graphic Design out of Chicago, IL"><strong>Invex Design</strong></a></span>
    <ul class="responsive-info">
        <li>St. Charles East Music Boosters</li>
        <li>1020 Dunham Rd, St. Charles, IL 60174</li>
        <li><a href="mailto:stcemusicboosters@gmail.com">stcemusicboosters@gmail.com</a></li>
        <li>© <?php echo date("Y"); ?></li>
        <li style="color: #93a2ae; font-size: 11px;">Powered by <a href="http://www.invexdesign.com" target="_blank" title="Invex Design | Web Design, Web Hosting, SEO Consulting, Graphic Design out of Chicago, IL"><strong>Invex Design</strong></a></li>
     <ul>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>

