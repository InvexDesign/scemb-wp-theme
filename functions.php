<?php
require_once('includes/constants.php');
require_once('includes/helpers.php');

add_theme_support('menus');
add_theme_support('post-thumbnails');

if(!is_admin())
{
	add_action("wp_enqueue_scripts", "use_custom_jquery", 0);
}
function use_custom_jquery()
{
	wp_deregister_script('jquery');
	wp_register_script('jquery', "/wp-content/themes/scemb-wp-theme/node_modules/jquery/dist/jquery.min.js", false, null);
	wp_enqueue_script('jquery');
}

function enqueueAssets()
{
	wp_register_style('app-css', get_template_directory_uri() . '/assets/build/css/app.min.css');
	wp_enqueue_style('app-css');

	wp_register_script('app-js', get_template_directory_uri() . '/assets/build/js/bundle.min.js');
	wp_enqueue_script('app-js');
}

add_action('wp_enqueue_scripts', 'enqueueAssets', 0);

// Custom TinyMCE Editor Styles
function wpb_mce_buttons_2($buttons)
{
    array_unshift($buttons, 'styleselect');

    return $buttons;
}

add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

function my_mce_before_init_insert_formats($init_array)
{
    $style_formats = [
        [
            'title'    => 'PDF Link',
            'inline'   => 'a',
            'selector' => 'a',
            'classes'  => 'pdf',
            'wrapper'  => false,

        ],
        [
            'title'    => 'PDF List',
            'block'    => 'ul',
            'selector' => 'ul',
            'classes'  => 'pdf-list',
            'wrapper'  => true,
        ],
    ];
    $init_array['style_formats'] = json_encode($style_formats);

    return $init_array;

}
add_filter('tiny_mce_before_init', 'my_mce_before_init_insert_formats');

add_editor_style('custom-editor-styles.css');
