<?php
/*
Template Name: Full-Width
*/
?>

<?php get_header(); ?>

<div id="content">
    <?php while ( have_posts() ) : the_post(); ?>
        <h2><?php echo the_title(); ?></h2>
        <?php the_content(); ?>
        <div class="clearer"> </div>
    <?php endwhile; ?>
</div>

<?php get_footer(); ?>
