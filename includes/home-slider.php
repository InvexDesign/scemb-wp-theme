<?php
$slides = getMetaSlides(HOME_SLIDER_ID);
?>
<div id="cycle">
	<span id="banner-nav"></span>
	<div id="banners">
	<?php foreach($slides as $slide) : ?>
		<div class="slide"><img src="<?php echo $slide['image_url']; ?>" /></div>
	<?php endforeach; ?>
</div>

<script type="text/javascript">
	$('#banners').cycle({
		pager: '#banner-nav',
		timeout: 8500,
		speed: 1500,
		fx: 'fade'
	});
</script>