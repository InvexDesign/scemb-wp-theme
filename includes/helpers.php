<?php

function getAdvancedCustomFieldValue($field, $default = null)
{
	$field = get_field_object($field);
	if($field && $field['value'])
	{
		return $field['value'];
	}

	return $default;
}

function getMetaSlides($metaslider_id)
{
	$args = [
		'force_no_custom_order' => true,
		'orderby'               => 'menu_order',
		'order'                 => 'ASC',
		'post_type'             => ['attachment', 'ml-slide'],
		'post_status'           => ['inherit', 'publish'],
		'lang'                  => '', // polylang, ingore language filter
		'suppress_filters'      => 1, // wpml, ignore language filter
		'posts_per_page'        => -1,
		'tax_query'             => [
			[
				'taxonomy' => 'ml-slider',
				'field'    => 'slug',
				'terms'    => $metaslider_id
			]
		]
	];

	$slides = [];
	$args = apply_filters( 'metaslider_populate_slides_args', $args, $metaslider_id, []);
	$query = new WP_Query( $args );
	while($query->have_posts())
	{
		$query->next_post();
		$post = $query->post;
		$featured_image = get_the_post_thumbnail_url($post->ID);
		$slides[] = [
			'image_url' => $featured_image ?: $post->guid,
			'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
			'content'   => $post->post_excerpt,
			'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
			'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
		];
	}
	wp_reset_postdata();

	return $slides;
}