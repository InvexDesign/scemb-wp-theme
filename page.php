<?php get_header(); ?>

<div id="content" class="secondary-container">
    <div class="secondary-wrap">
        <?php while ( have_posts() ) : the_post(); ?>
            <h2><?php echo the_title(); ?></h2>
            <?php the_content(); ?>
            <div class="clearer"> </div>
        <?php endwhile; ?>
    </div>
    <div id="sidebar">
        <?php the_field('sidebar-content'); ?>
    </div>
</div>

<?php get_footer(); ?>
