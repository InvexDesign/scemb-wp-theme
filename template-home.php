<?php /* Template Name: Home */ ?>

<?php get_header(); ?>

<?php if(have_posts()) : ?>
	<?php while(have_posts()) : the_post(); ?>

		<?php
			$base_image_url = get_template_directory_uri() . '/assets/build/images/';
			$image_one = getAdvancedCustomFieldValue('image-one', $base_image_url . 'band.png');
			$image_two = getAdvancedCustomFieldValue('image-two', $base_image_url . 'orchestra.jpg');
			$image_three = getAdvancedCustomFieldValue('image-three', $base_image_url . 'choir.jpg');
		?>

		<div class="banner-wrap">
			<div class="intro"><?php the_content(); ?></div>
			<div class="pic">
				<img src="<?php echo $image_one; ?>" />
				<img src="<?php echo $image_two; ?>" />
				<img src="<?php echo $image_three; ?>" />
			</div>
		</div>

		<div id="content" class="index no-border">
			<a href="<?php echo home_url('/'); ?>band"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/button-band.jpg" /></a>
			<a href="<?php echo home_url('/'); ?>choir"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/button-choir.jpg" /></a>
			<a href="<?php echo home_url('/'); ?>orchestra"><img src="<?php echo get_template_directory_uri(); ?>/assets/build/images/button-orchestra.jpg" /></a>
		</div>

	<?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>
